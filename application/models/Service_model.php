<?php
 class Service_model extends CI_Model {
	protected $table1			=	'service_office';
	protected $table2			=	'service_category';
	//protected $table3			=	'service_sub_category';
	protected $table4			=	'services';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllServiceOfficeData($limit,$offset){
	    $this->db->select('*');
		$this->db->from('service_office');
		$this->db->order_by('service_office.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertServiceOfficeData($params){ 
		$ins		  =	$this->db->insert($this->table1,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function getUpdateServiceOfficeData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table1,$params['condition']);
		return $query->result_array();		
	}
	public function rowWiseData($editId)
	{
		$query 	= $this->db->query("SELECT imageUrl FROM news where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	} 
	public function updateServiceOfficeAction($params,$editId)
	{
		$condition=array('id'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table1,$params);	
		return $up;
	}
	public function deleteSeriveOfficeData($id) { 
    	if ($this->db->delete($this->table1, "id = ".$id)) { 
            return true; 
         } 
    } 
    
    
    // * ********************** Service Category ********************* //
    
    public function getAllServiceCategoryData($limit,$offset){
	    $this->db->select('service_office.*,service_category.*,service_category.id as sCatId');
		$this->db->from('service_category');
		$this->db->join('service_office','service_office.id=service_category.officeId');
		$this->db->order_by('service_category.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertServiceCategoryData($params){ 
		$ins		  =	$this->db->insert($this->table2,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	
	//get office in select box
	public function getOffice(){
		$query	= $this->db->get($this->table1);
		return $query->result();
	}
	
	public function getUpdateServiceCategoryData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table2,$params['condition']);
		return $query->result_array();		
	}
	
	public function updateServiceCategoryAction($params,$editId)
	{
		$condition=array('id'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table2,$params);	
		return $up;
	}
	public function deleteSeriveCategoryData($id) { 
    	if ($this->db->delete($this->table2, "id = ".$id)) { 
            return true; 
         } 
    } 
    
    
     // * ********************** Service ********************* //
    
    public function getAllServiceData($limit,$offset){
	    $this->db->select('services.*,services.id as sId,service_category.*');
		$this->db->from('services');
		$this->db->join('service_category','service_category.id=services.categoryId');
		$this->db->order_by('services.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertServiceData($params){ 
		$ins		  =	$this->db->insert($this->table4,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	
	//get category in select box
	public function getServiceCategory(){
		$query	= $this->db->get($this->table2);
		return $query->result();
	}
	
	public function getUpdateServiceData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table4,$params['condition']);
		return $query->result_array();		
	}
	
	public function updateServiceAction($params,$editId)
	{
		$condition=array('id'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table4,$params);	
		return $up;
	}
	public function deleteSeriveData($id) { 
    	if ($this->db->delete($this->table4, "id = ".$id)) { 
            return true; 
         } 
    }
    
    public function getAllDatas($id) {
		$this->db->select('services.id as servId,service_category.id as serCatId');
			$this->db->from('services');
			$this->db->join('service_category','services.categoryId = service_category.id');
			$this->db->join('service_office','service_category.officeId = service_office.id');
			$this->db->where('service_category.officeId='.$id);
			
			$query = $this->db->get();
			
			//echo $this->db->last_query();die;
			return $query->result();
	}
	
	public function deleteAllServiceData($serviceId) { 
    	if ($this->db->delete($this->table4, "id = ".$serviceId)) { 
            return true; 
         } 
    }
    /*public function deleteSubCatData($serviceSubId) { 
    	if ($this->db->delete($this->table3, "id = ".$serviceSubId)) { 
            return true; 
         } 
    }*/
    public function deleteCatId($serviceCatId) { 
    	if ($this->db->delete($this->table2, "id = ".$serviceCatId)) { 
            return true; 
         } 
    }
    
    public function deleteOfficeData($id) { 
    	if ($this->db->delete($this->table1, "id = ".$id)) { 
            return true; 
         } 
    }
	
	public function getAllCategoryData($id) {
		$this->db->select('services.id as servId');
		$this->db->from('service_category');
		$this->db->join('services','services.categoryId=service_category.id');
		$this->db->where('services.categoryId='.$id);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	
	 
    
    public function deleteServDataFromCat($servId) { 
    	if ($this->db->delete($this->table4, "id = ".$servId)) { 
            return true; 
         } 
    }
    
    public function getAllSubCategoryData($id) {
		$this->db->select('services.id as serviceId');
		$this->db->from('service_sub_category');
		$this->db->join('services','services.subCategoryId=service_sub_category.id');
		$this->db->where('services.subCategoryId='.$id);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function deleteServiceFromSubCate($servId) { 
    	if ($this->db->delete($this->table4, "id = ".$servId)) { 
            return true; 
         } 
    }
	
	
    
}