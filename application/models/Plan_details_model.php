<?php
 class Plan_details_model extends CI_Model {
	protected $table='plan_details';
	protected $table1='plan_category';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	    $this->db->select('plan_details.*,plan_category.*,plan_details.id as plandId');
		$this->db->from('plan_details');
		$this->db->join('plan_category','plan_category.id=plan_details.categoryId');
		$this->db->order_by('plan_details.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	
	//get category in select box
	public function getCategory(){
		$query	= $this->db->get($this->table1);
		return $query->result();
	}
 	
	public function insertData($params){ 
		$ins	 =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function getUpdateData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table,$params['condition']);
		//echo $this->db->last_query();die;
		return $query->result_array();		
	}
	public function rowWiseData($editId)
	{
		$query 	= $this->db->query("SELECT imageUrl FROM news where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	} 
	public function updateAction($params,$editId)
	{
		$condition=array('id'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
    } 

    //check plan details is there or not from plan category
    public function deleteDetailsData($id) { 
    	if ($this->db->delete($this->table, "categoryId = ".$id)) { 
            return true; 
         } 
    }

	
}