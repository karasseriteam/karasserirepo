<?php
/**
* 
*/
class Representative_model extends CI_Model
{
	protected $table = 'representatives';
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}

	// insert methode
	public function insertData($params){
		$insert = $this->db->insert($this->table,$params);
		return $insert;
	}

	//gett all details from representative table
	public function getAllData($limit,$offset){
		//$this->db->order_by('ID','ASC');
		$this->db->from($this->table);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		return $query->result();
	}

	//getdata for edit
	public function getUpdateData($data){
		$this->db->select($data['fields']);
		$query	=	$this->db->get_where($this->table,$data['condition']);
		return $query->result_array();
	}

	public function updateAction($params,$editId)
	{
	 	$condition=array('ID'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	public function rowWiseData($editId)
    {
	  	$query 	= $this->db->query("SELECT photo FROM representatives where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	}
	 public function deleteData($id) { 
         if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
         } 
      } 
       public function getRepData(){
	    $this->db->select('*');
		$this->db->from('representatives');
		$this->db->order_by('name');		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	} 
 
       //checking the wardno is already exists or not
	public function wardno_exist($wardNo) {
		$this->db->where('wardNumber='.$wardNo);
		$query= $this->db->get('representatives');
	  	$num = $query->num_rows();
	  	return $num;
	}
}