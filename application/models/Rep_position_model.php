<?php
defined('BASEPATH') OR exit("No direct script access allowed");

class Rep_position_model extends CI_model
{
	protected $table='rep_position';
	protected $table2='positions';
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//gett all details from representative table
	public function getAllData($limit,$offset){
		 $this->db->select('rep_position.*,representatives.*,positions.*,rep_position.ID as rep_pos_id,representatives.ID as rep_id,positions.id as pos_id');
			$this->db->from('rep_position');
			$this->db->join('representatives','rep_position.representativeId = representatives.ID');	
			$this->db->join('positions','rep_position.positionId = positions.ID');	
			$this->db->order_by('rep_position.ID','Asc');
            $this->db->limit($limit, $offset);
			$query = $this->db->get();
			return $query->result();
	}

	// insert methode
	public function insertData($params){
		$insert = $this->db->insert($this->table,$params);
		return $insert;
	}

	//for getting data from the defined table
	public function getDataFrom($table)
	{
		if($table==$this->table2){
			$this->db->limit('2');
		}
		$query = $this->db->get($table);
		return $query->result();
	}

	//check whether the position is already allocated or not
	public function checkExisting($positionid){
		//$this->db->where('positionId',$positionid);
		$query = $this->db->get_where($this->table, array('positionId' =>$positionid));
		return $query->num_rows();
	}

	//updating position 
	public function updateData($params,$position){
		$this->db->where('positionId',$position);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
}