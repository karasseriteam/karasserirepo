<?php
 class Pmc_model extends CI_Model {
	protected $table='pmc';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData($limit,$offset){
	    $this->db->select('*');
		$this->db->from('pmc');
		$this->db->order_by('pmc.id','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function getUpdateData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table,$params['condition']);
		return $query->result_array();		
	}
	public function rowWiseData($editId)
	{
		$query 	= $this->db->query("SELECT imageUrl FROM news where id='$editId'");
		$row 	= $query->row();
		//echo $this->db->last_query();die;
		return $row;
	} 
	public function updateAction($params,$editId)
	{
		$condition=array('id'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "id = ".$id)) { 
            return true; 
         } 
    } 
	
}