<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->helper('form');

        
	}

	public function index(){
		$this->load->view('admin/index.php');
	}

	public function login(){

		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		//try{
			if ($this->form_validation->run() == false) {
				redirect('Login');
			}
			else{

				$username = $this->input->post('username');
				$password = $this->input->post('password');

				if($this->login_model->userlogin($username,$password))
				{
					$userid = $this->login_model->get_user_id($username);
					$user 	= $this->login_model->get_user($userid); 

					$_SESSION['userid']       = (int)$user->id;
					$_SESSION['username'] 	  = (string)$user->username;
					$_SESSION['logged_in']    = (bool)true;
                    $_SESSION['is_confirmed'] = (bool)true;
                    $_SESSION['user_type']    = (string)$user->usertype;
                    
                    redirect('dashboard');
                }
                else
                {
                	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Wrong username or password"]);
                	
                	redirect('Login');
                }
			}
		
	}

	public function logout()
	{
		if(isset($_SESSION['logged_in'])&&$_SESSION['logged_in']==true){
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
				redirect('Login');
			}
		}
		else{
			redirect('Login');
		}
	}

	// change password
	public function changePasswordForm () {
        if (@$_SESSION['logged_in']) {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->view('admin/header');
            $this->load->view('admin/change_password/index.php');
            $this->load->view('admin/footer');
        } else {
            redirect('login');
        }
    }
    public function changePassword () {
        $this->load->helper('form');
        $this->load->library('form_validation');

        // set validation rules
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('nPassword', 'New Password', 'required');
        $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required');
        if ($this->form_validation->run() == false) {
            // validation not ok, send validation errors to the view
            $this->load->view('admin/header');
            $this->load->view('admin/change_password/index');
            $this->load->view('admin/footer');
        } else {
            $currentPassword = $this->input->post('password');
            $newPassword = $this->input->post('nPassword');
            $confirmPassword = $this->input->post('cPassword');
            if ($newPassword != $confirmPassword) {
                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Password mismatch!"]);
                $this->load->view('admin/header');
                $this->load->view('admin/change_password/index');
                $this->load->view('admin/footer');
            } else {
                $user = $this->login_model->get_user($_SESSION['userid']);
                if ($this->login_model->checkPasswordMatch($currentPassword, $user->password)) {
                    if ($this->login_model->updatePassword($_SESSION['userid'], $newPassword)) {
                        redirect('logout');
                    } else {
                        $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Something went wrong! Please try again."]);
                        $this->load->view('admin/header');
                        $this->load->view('admin/change_password/index');
                        $this->load->view('admin/footer');
                    }
                } else {
                    $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Current password is incorrect!"]);
                    $this->load->view('admin/header');
                    $this->load->view('admin/change_password/index');
                    $this->load->view('admin/footer');
                }
            }
        }
    }
}