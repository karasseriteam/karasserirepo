<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan_details extends CI_Controller {
	protected $baseFolder		=	'admin/plan_details';
	protected $table			=	'plan_details';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('Plan_details_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
        if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	public function index() {  
		
		$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Plan_details/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->Plan_details_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function add_view(){
     	
     	$data['category'] = $this->Plan_details_model->getCategory();
     	
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add",$data);
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$cat_id = $this->input->post('category');
	 	
	 	$heading				= NULL;
	 	$link					= NULL;
     	$submit 				= NULL;
     	
     	extract($_POST);
     	$params['categoryId']		=	$cat_id;  
     	$params['heading']			=	$heading;  
     	$params['link']				=	$link;  
     	
     	if(isset($submit))
     	{		
			$res=$this->Plan_details_model->insertData($params);			
			 if($res)
	         {
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Plan_details/index');		
	 }
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'id',
		'categoryId',
		'heading',
		'link'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'id'=>$tableId
         );  
         	    
         $data['results']=$this->Plan_details_model->getUpdateData($data);
         //print_r($data['results']);die;
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId			= NULL;
	 	$category		= $this->input->post('category');
	 	$heading = NULL;
	 	$link   = NULL;
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$params['categoryId']			=	$category; 
     	$params['heading']			=	$heading; 
     	$params['link']			=	$link; 
     	   	     	
     	if(isset($submit))
     	{			
			$res=$this->Plan_details_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('Plan_details/index');
	 }
  	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Plan_details_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Plan_details/index');  		
      }
}
