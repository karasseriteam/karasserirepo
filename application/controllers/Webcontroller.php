<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Webcontroller extends REST_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('Web_model');
		$this->load->helper('url');			
	}
	
	function ad_get()
	{
		$results=$this->Web_model->fetchAll('ad');
		if($results)
		{
            $i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('ad'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function emergencyContacts_get()
	{
		$results=$this->Web_model->fetchAll('emergency_contacts');
		if($results)
		{
			$this->response(array('emergencyContacts'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function enquiry_get()
	{
		$results=$this->Web_model->fetchAll('enquiry');
		if($results)
		{
			$this->response(array('enquiries'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function postEnquiry_post()
	{
/*$headers = apache_request_headers();

		foreach ($headers as $header => $value)
		{
		    echo "$header: $value <br />\n";
		 
		}*/
	//{"name":"asnad","mobileNumber":"9746410718",,"enquiryDescription":"description"," aadhaarNumber":"12345"}	
		$params = json_decode(file_get_contents('php://input'), TRUE);
		//print_r($params);
		$eDate=date('Y-m-d');
        $user = array(
            'userName'  	=> $params['name'],
            'phone' 		=> $params['mobileNumber'],
            'description' 	=> $params['enquiryDescription'],
            'aadhar' 		=> $params['aadhaarNumber'],
            'eDate'		=> $eDate	
        ); 
        //print_r($user);
        $result = $this->Web_model->insertData($user);
        if ($result) {
            //$this->response($result, 200); // 200 being the HTTP response code
            $this->response(array('status'=>'Success','message'=>'Thank you for your feedback. We will get back you soon'),200);
        } else {
            $this->response(NULL, 404);
               }
	} 
        function scroll_get()
	{
		$results=$this->Web_model->fetchAll('flash_news');
		if($results)
		{
			$i = 0;$scrollNews='';
			$count=count($results);
			foreach($results as $r)
			{
				$scrollNews 	= $scrollNews . $r['news'];
				$i += 1;
				if($count!=$i)
				$scrollNews		=	$scrollNews." - ";
			}
			$this->response(array('scrollNews'=>$scrollNews,'status'=>'Success','message'=>'News loaded successfully'));
		}
		
	}
	function gallery_get()
	{
		$results=$this->Web_model->fetchAll('gallery');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results2[$i]['categoryId'] 		 = $r['categoryId'];
				$results2[$i]['categoryImageUrl']    = base_url().$r['categoryImageUrl'];
				$results2[$i]['categoryDescription'] = $r['categoryDescription'];
				$results2[$i]['imageId'] 		 	 = $r['imageId'];
				$results2[$i]['imageUrl']    		 = base_url().$r['imageUrl'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('galleryImages'=>$results2,'status'=>'Success','message'=>'Data loaded successfully'));
		}	
	}
	function hotspot_get()
	{
		$results=$this->Web_model->fetchAll('hotspot');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				unset($results[$i]['photo']);
				unset($results[$i]['photo2']);
				unset($results[$i]['photo3']);
				/*$pics = array (
				array("imageId" => "1", "imageUrl" => base_url().$r['photo']),
				array("imageId" => "1", "imageUrl" => base_url().$r['photo2']),
				array("imageId" => "1", "imageUrl" => base_url().$r['photo3'])
				);*/

				//$results[$i]['photo'] = base_url().$r['photo'];
				//$results[$i]['photo2'] = base_url().$r['photo2'];
				//$results[$i]['photo3'] = base_url().$r['photo3'];

                               $pics=array();
                $pic=array();
				if($r['photo']){
					$pic['imageId'] ="1";
					$pic['imageUrl']=base_url().$r['photo'];
					array_push($pics,$pic);
				}
				if($r['photo2']){
					$pic['imageId'] ="1";
					$pic['imageUrl']=base_url().$r['photo2'];
					array_push($pics,$pic);
				}
				if($r['photo3']){
					$pic['imageId'] ="1";
					$pic['imageUrl']=base_url().$r['photo3'];
					array_push($pics,$pic);
				}
				$results[$i]['images'] = $pics;
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('hotspots'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
			
		}
		
	}
	function maps_get()
	{
		$results=$this->Web_model->fetchAll('map');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('maps'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function medical_equipment_get()
	{
		$results=$this->Web_model->fetchAll('medical_equipment');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('medical_equipment'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function news_get()
	{
		$results=$this->Web_model->fetchAll('news');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$newsDate				=	$r['newsDate'];
				$frontendDate 				= 	date('d-M-Y', strtotime($newsDate));
				
				$results2[$i]['id'] 	 		= 	$r['id'];
				$results2[$i]['heading'] 		= 	$r['heading'];
				$results2[$i]['description'] 	        = 	$r['description'];
				$results2[$i]['date'] 	 		= 	$frontendDate;
				$results2[$i]['imageUrl'] 		= 	base_url().$r['imageUrl'];
				$i += 1;
			}
			$this->response(array('news'=>$results2,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}	
	function palliative_get()
	{
		$results=$this->Web_model->fetchAll('palliative');
		if($results)
		{
			$this->response(array('palliative'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function plan_category_get()
	{
		$results=$this->Web_model->fetchAll('plan_category');
		if($results)
		{
			$this->response(array('plan_category'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function plans_get()
	{
		$results=$this->Web_model->fetchAll('plan_details');
		if($results)
		{
			$this->response(array('plans'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function pmc_get()
	{
		$results=$this->Web_model->fetchAll('pmc');
		if($results)
		{
			$this->response(array('pmc'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function positions_get()
	{
		$results=$this->Web_model->fetchAll('positions');
		if($results)
		{
			$this->response(array('positions'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function representatives_get()
	{
		$results=$this->Web_model->fetchAll('representatives');
		if($results)
		{
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['photo'] = base_url().$r['photo'];
				//$img=$r['photo'];echo $img;die;
				$i += 1;
			}
			$this->response(array('representatives'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function rep_position_get()
	{
		$results=$this->Web_model->fetchAll('rep_position');
		if($results)
		{
			$this->response(array('rep_position'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
        function services_get()
	{
		$results=$this->Web_model->fetchAll('services');//print_r($results);
		if($results){	
			$i = 0;
			foreach($results as $r)
			{
				$results[$i]['heading'] = $r['subCategory'];
				$i += 1;
			}	
			$this->response(array('services'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
	
	}
	function service_category_get()
	{
		$results=$this->Web_model->fetchAll('service_category');
		if($results)
		{
			$this->response(array('service_category'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function service_office_get()
	{
		$results=$this->Web_model->fetchAll('service_office');
		if($results)
		{
			$this->response(array('service_office'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function service_sub_category_get()
	{
		$results=$this->Web_model->fetchAll('service_sub_category');
		if($results)
		{
			$this->response(array('service_sub_category'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}function committee_get()
	{
		$results=$this->Web_model->fetchAll('standing_committee');
		if($results)
		{
			$this->response(array('committee'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	function standingCommittee_get()
	{
		$respo = array();$i=$j=0;
		$results1=$this->Web_model->fetchAll('rep_position');
		foreach($results1 as $r)
		{	
			$respo[$j]['id'] 				  = 0;
			$respo[$j]['standingCommittee']   = "Panchayath";
			$respo[$j]['representativeId']    = $r['representativeId'];
			$respo[$j]['name']  			  = $r['name'];
			$respo[$j]['gender']  		  	  = $r['gender'];
			$respo[$j]['age']  			      = $r['age'];
			$respo[$j]['address']  		      = $r['address'];
			$respo[$j]['phone']  			  = $r['phone'];
			$respo[$j]['mobile']  		      = $r['mobile'];
			$respo[$j]['maritalStatus']  	  = $r['maritalStatus'];
			$respo[$j]['eduQualification']    = $r['eduQualification'];
			$respo[$j]['job']  			      = $r['job'];
			$respo[$j]['party']  			  = $r['party'];
			$respo[$j]['wardNumber']  	      = $r['wardNumber'];
			$respo[$j]['wardName']  		  = $r['wardName'];
			$respo[$j]['photo']  			  = base_url().$r['photo'];
			$respo[$j]['position']  		  = $r['position'];	
			$j++;				
		}
		$results=$this->Web_model->fetchAll('standing_committee');
		foreach($results as $r)
		{			
			$id						   =	$r['id'];			
			$results2  				   =	$this->Web_model->committeeDetails($id);
			if($results2)
			//array_push($respo, $results2);			
			foreach($results2 as $r)
			{		
				
				$respo[$j]['id'] 				  = $r['id'];
				$respo[$j]['standingCommittee']   = $r['standingCommittee'];
				$respo[$j]['representativeId']    = $r['representativeId'];
				$respo[$j]['name']  			  = $r['name'];
				$respo[$j]['gender']  		      = $r['gender'];
				$respo[$j]['age']  			      = $r['age'];
				$respo[$j]['address']  		      = $r['address'];
				$respo[$j]['phone']  			  = $r['phone'];
				$respo[$j]['mobile']  		      = $r['mobile'];
				$respo[$j]['maritalStatus']  	  = $r['maritalStatus'];
				$respo[$j]['eduQualification']    = $r['eduQualification'];
				$respo[$j]['job']  			      = $r['job'];
				$respo[$j]['party']  			  = $r['party'];
				$respo[$j]['wardNumber']  	      = $r['wardNumber'];
				$respo[$j]['wardName']  		  = $r['wardName'];
				$respo[$j]['photo']  			  = base_url().$r['photo'];
				$respo[$j]['position']  		  = $r['position'];		
				$j++;				
			}
		}
		$this->response(array('committees'=>$respo,'status'=>'Success','message'=>'Data loaded successfully'));	
	}
	function statistics_get()
	{
		$results=$this->Web_model->fetchAll('statistics');
		if($results)
		{
			$this->response(array('statistics'=>$results,'status'=>'Success','message'=>'Data loaded successfully'));
		}
		
	}
	
}	