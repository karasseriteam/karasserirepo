<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
	protected $baseFolder1		=	'admin/service_office';
	protected $baseFolder2		=	'admin/service_category';
	protected $baseFolder3		=	'admin/service_sub_category';
	protected $baseFolder4		=	'admin/service';
	protected $table1			=	'service_office';
	protected $table2			=	'service_category';
	protected $table3			=	'service_sub_category';
	protected $table4			=	'services';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('Service_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	public function service_office_index() {  
		
		$num_rows=$this->db->count_all("$this->table1");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Service/service_office_index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->Service_model->getAllServiceOfficeData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder1/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function service_office_add_view(){
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder1/add");
        $this->load->view("$this->footer");
	 }	
	 public function service_office_add()
	 {
	 	$office				= NULL;
     	$submit 			= NULL;
     	
     	extract($_POST);
     	$params['office']			=	$office;  
     	
     	if(isset($submit))
     	{		
			$res=$this->Service_model->insertServiceOfficeData($params);			
			 if($res)
	         {
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Service/service_office_index');		
	 }
	 public function edit_service_office_view()
	 {
	 	$data['fields']=array(
		'id',
		'office'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'id'=>$tableId
         );  
         	    
         $data['results']=$this->Service_model->getUpdateServiceOfficeData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder1/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit_service_office()
	 {	 
	 	$editId			= NULL;
	 	$office				= NULL;
     	$submit 			= NULL;
     	
     	extract($_POST);
     	$params['office']			=	$office;
     	   	     	
     	if(isset($submit))
     	{			
			$res=$this->Service_model->updateServiceOfficeAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('Service/service_office_index');
	 }
  	public function service_office_delete() { 
         $id = $this->uri->segment('3'); 
         //$res=$this->Service_model->deleteSeriveOfficeData($id);
         $res1 = $this->Service_model->getAllDatas($id); //print_r($res1);die;
         
         foreach($res1 as $r) {
		 	$serviceId = $r->servId;//echo $serviceId;
		 	//$serviceSubId = $r->srvSubId;//echo $serviceSubId;
		 	$serviceCatId = $r->serCatId;//echo $serviceCatId;
		 	
		 	$res1 = $this->Service_model->deleteAllServiceData($serviceId);
		 	//$res2 = $this->Service_model->deleteSubCatData($serviceSubId);
		 	$res3 = $this->Service_model->deleteCatId($serviceCatId);
		 	
		 	}
         //die;
         $res4 = $this->Service_model->deleteOfficeData($id);
         	
         if($res4)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 	}
		 
        redirect('Service/service_office_index');  		
      }
      
      // * ************** Service Category ************************* //
      
      
      public function service_category_index() {  
		
		$num_rows=$this->db->count_all("$this->table2");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Service/service_category_index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->Service_model->getAllServiceCategoryData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder2/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function service_category_add_view(){
     	
     	$data['office'] = $this->Service_model->getOffice();
     	
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder2/add",$data);
        $this->load->view("$this->footer");
	 }	
	 public function service_category_add()
	 {
	 	$officeId			= $this->input->post('officeId');
	 	$category			= NULL;
     	$submit 			= NULL;
     	
     	extract($_POST);
     	$params['officeId']			=	$officeId; 
     	$params['category'] 		= 	$category;
     	
     	if(isset($submit))
     	{		
			$res=$this->Service_model->insertServiceCategoryData($params);			
			 if($res)
	         {
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Service/service_category_index');		
	 }
	 public function edit_service_category_view()
	 {
	 	$data['fields']=array(
		'id',
		'officeId',
		'category'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'id'=>$tableId
         );  
         	    
         $data['results']=$this->Service_model->getUpdateServiceCategoryData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder2/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit_service_category()
	 {	 
	 	$editId			= NULL;
	 	$officeId				=$this->input->post('officeId');
	 	$category				= NULL;
     	$submit 			= NULL;
     	
     	extract($_POST);
     	$params['officeId']			=	$officeId;
     	$params['category']			=	$category;
     	   	     	
     	if(isset($submit))
     	{			
			$res=$this->Service_model->updateServiceCategoryAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('Service/service_category_index');
	 }
  	public function service_category_delete() { 
         $id = $this->uri->segment('3'); 
         
         $res = $this->Service_model->getAllCategoryData($id);
         
         foreach($res as $r1){
		 	//$subCatId = $r1->subId;
		 	$servId = $r1->servId;
		 	
		 	//$res2 = $this->Service_model->deleteSubCatDataFromCat($subCatId);
		 	$res3 = $this->Service_model->deleteServDataFromCat($servId);
		 }
		 
         $res=$this->Service_model->deleteSeriveCategoryData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Service/service_category_index');  		
      }
      
      
      
       // * ************** Service ************************* //
      
      
      public function service_index() {  
		
		$num_rows=$this->db->count_all("$this->table4"); //echo $num_rows;die;
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Service/service_index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->Service_model->getAllServiceData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder4/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function service_add_view(){
     	
     	$data['category'] = $this->Service_model->getServiceCategory();
     	
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder4/add",$data);
        $this->load->view("$this->footer");
	 }	
	 public function service_add()
	 {
	 	$categoryId			= $this->input->post('categoryId');
	 	$subCategory		= NULL;
	 	//$heading			= NULL;
	 	$description			= NULL;
	 	$link			= NULL;
     	$submit 			= NULL;
     	
     	extract($_POST);
     	$params['categoryId']		=	$categoryId; 
     	$params['subCategory']		= 	$subCategory;
     	//$params['heading'] 			= 	$heading;
     	$params['description'] 		= 	$description;
     	$params['link'] 			= 	$link;
     	
     	if(isset($submit))
     	{		
			$res=$this->Service_model->insertServiceData($params);			
			 if($res)
	         {
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Service/service_index');		
	 }
	 public function edit_service_view()
	 {
	 	$data['fields']=array(
		'id',
		'categoryId',
		'subCategory',
		'description',
		'link'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'id'=>$tableId
         );  
         	    
         $data['results']=$this->Service_model->getUpdateServiceData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder4/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit_service()
	 {	 
	 	$editId				= NULL;
	 	$categoryId			= $this->input->post('categoryId');
	 	$subCategory		= NULL;
	 	//$heading			= NULL;
	 	$description		= NULL;
	 	$link				= NULL;
     	$submit 			= NULL;
     	
     	extract($_POST);
     	$params['categoryId']		=	$categoryId;
     	$params['subCategory']		=	$subCategory; 
     	//$params['heading'] 			= 	$heading;
     	$params['description'] 		= 	$description;
     	$params['link'] 			= 	$link;
     	   	     	
     	if(isset($submit))
     	{			
			$res=$this->Service_model->updateServiceAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('Service/service_index');
	 }
  	public function service_delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Service_model->deleteSeriveData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Service/service_index');  		
      }
      
}
