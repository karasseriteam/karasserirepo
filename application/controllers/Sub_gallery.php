<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_gallery extends CI_Controller {
	protected $baseFolder		=	'admin/sub_gallery';
	protected $table			=	'sub_gallery';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('Sub_gallery_model','Gallery_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	
    	
	 public function add_sub_image()
	 {  
	 	$galleryId 		= $this->input->post('galleryId');
     	$submit 		= NULL;
     	extract($_POST);
     	
     	$galleryCount=$this->Gallery_model->getGalleryCount();
     	$subGallleryCount=$this->Sub_gallery_model->getSubCount();
     	$imageTotal=$galleryCount+$subGallleryCount;
     	$subImageCount=$this->Sub_gallery_model->getCount($galleryId);//echo $subImageCount;
     	
     	if($subImageCount<5 && $imageTotal<30){
			$params['galleryId'] = $galleryId;
     	
     		//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./images/sub_gallery/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
          $this->upload->initialize($up_image);
          if($this->upload->do_upload('subImage')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['subImage']  	=	"/images/sub_gallery/".$fileUpload['file_name'];
		  } 
     	  //echo $fileUpload['file_name'];die; 
     	 
	     	if(isset($submit))
	     	{		
				$res=$this->Sub_gallery_model->insertData($params);			
				 if($res)
		         {
		         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
				 }
				 else{
				 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
				 }
			}
		}
		else if($subImageCount>=5){
			$this->session->set_flashdata("flash", ["type" => "danger", "message" => "You can add only 5 sub images..!"]);
		}
     	else if($imageTotal>=30){
			$this->session->set_flashdata("flash", ["type" => "danger", "message" => "You can add only 30 sub images..!"]);
		}
		 redirect('Gallery/index');		
	 }
  	public function delete() { 
         $id = $this->uri->segment('3'); 

         $res2=$this->Sub_gallery_model->rowWiseData2($id);
     		if (isset($res2))
			{
			        $img= $res2->subImage;
			        $imageName = ltrim($img, '/');
			        unlink($imageName);
			}

         $res=$this->Sub_gallery_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Gallery/index');  		
      }
}
