<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Position extends CI_Controller
{
	protected $baseFolder		=	'admin/position';
	protected $table			=	'positions';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';

	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model('Position_model');
$this->load->library('session');
                if(empty($this->session->userdata("userid")))
                  {
        	   $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	   redirect(site_url(),'refresh');
                  }
	}

	function index(){
		if (@$_SESSION['logged_in']) {
        $num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/position/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);

        $data['records'] = $this->Position_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        //$this->load->view('home/home_view',$data);
        $this->load->view("$this->footer");
        } else {
            redirect(base_url().'index.php/login');
        }
	}

	public function add(){
		$position			= NULL;   	
     	$submit 			= NULL;
     	$this->form_validation->set_rules('position','Position','required');
     	if($this->form_validation->run()==false){
			$this->load->view('admin/header');
			$this->load->view('admin/position/add.php');
			$this->load->view('admin/footer');
		}
		else{
	     	extract($_POST);
	     	$params['position']		=	$position;	
	     	if(isset($submit))
	     	{		
				$result = $this->Position_model->insertData($params);			
				 if($result)
		         {
		         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
				 }
				 else{
				 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
				 }
				 redirect('position/index');	
			}
			
		}
	}

	public function edit(){
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Position_model->getUpdateData($data);

         $this->load->view('admin/header');
		 $this->load->view('admin/position/edit',$data);
		 $this->load->view('admin/footer');

		$editId			= Null;
		$position 		= Null;

		extract($_POST);
		$params['position'] 		= $position; 
		if(isset($submit))
     	{
     		$res=$this->Position_model->updateAction($params,$editId);
			if($res)
	        {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			}
			else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			}
			redirect('position/index');
     	}
	}

	public function delete(){
		$id = $this->uri->segment('3'); 
        $res=$this->Position_model->deleteData($id); 
        if($res)
        {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		}
		else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		}
        redirect('position/index'); 
	}
}