<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	protected $baseFolder		=	'admin/news';
	protected $table			=	'news';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('News_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
        if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	public function index() {  
		
		$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/News/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->News_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function add_news(){
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add");
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$heading		= NULL;
     	$description	= NULL;
     	$newsDate     	= NULL;
     	//$setFlash		= NULL;
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$params['heading']	=	$heading;
     	$params['description']	 =	$description;     
     	$params['newsDate']	 =	date('Y-m-d', strtotime($newsDate));     
     	//$params['setFlash']	 = $setFlash;
     	//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./images/news/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
          $this->upload->initialize($up_image);
          if($this->upload->do_upload('imageUrl')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['imageUrl']  	=	"images/news/".$fileUpload['file_name'];
		  } 
     	 // echo $fileUpload['file_name'];die; 
     	 
     	if(isset($submit))
     	{		
			$res=$this->News_model->insertData($params);			
			 if($res)
	         {
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('News/index');		
	 }
	 public function edit_news()
	 {
	 	$data['fields']=array(
		'id',
		'heading',
		'description',
		'newsDate',
		'imageUrl'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->News_model->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId			= NULL;
	 	$heading		= NULL;
     	$description			= NULL;
     	$newsDate     	=NULL;
     	//$setFlash		= NULL;
     	$submit 		= NULL;
        $changeImage	= NULL;
     	
     	
     	extract($_POST);
     	$editId				 	=	$editId;
     	$params['heading']	=	$heading;
     	$params['description']	 =	$description;     
     	$params['newsDate']	 =	date('Y-m-d', strtotime($newsDate));
     	//$params['setFlash']	 = $setFlash;    
     	 
     	if($changeImage=='yes')
     	{
     		$res=$this->News_model->rowWiseData($editId);
     		if (isset($res))
			{
			        $img= $res->imageUrl;
			        
if (file_exists($img)) {
        unlink($img);
       }
			}
			//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./images/news',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);
            if($this->upload->do_upload('imageUrl')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		    } 
     	 // echo $fileUpload['file_name'];die; 
     	 $params['imageUrl']  	=	"images/news/".$fileUpload['file_name'];
		}
     	     	     	
     	if(isset($submit))
     	{			
			$res=$this->News_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('News/index');
	 }
  	public function delete_news() { 
         $id = $this->uri->segment('3'); 
         $res2=$this->News_model->rowWiseData($id);
     		if (isset($res2))
			{
			        $img= $res2->imageUrl;
			        
if (file_exists($img)) {
        unlink($img);
       }
			}
         $res=$this->News_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('News/index');  		
      }
}
