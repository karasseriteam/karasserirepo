<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Dashboard extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
                if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
	}

	public function index()
	{
		if(@$_SESSION['logged_in']&&@$_SESSION['user_type']=='admin'){
			$this->load->view('admin/header.php');
			$this->load->view('admin/dashboard/index.php');
			$this->load->view('admin/footer.php');
		}
	}
}
