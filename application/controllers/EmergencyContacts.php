<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmergencyContacts extends CI_Controller {
	protected $baseFolder		=	'admin/emergency_contacts';
	protected $table			=	'emergency_contacts';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('EmergencyContacts_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
        if(empty($this->session->userdata("userid")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect(site_url(),'refresh');
        }
      }
	public function index() {  
		
		$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/EmergencyContacts/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
	             
        $data['records'] = $this->EmergencyContacts_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function add_view(){
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add");
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$name		= NULL;
     	$address	= NULL; 
     	$phone		= NULL;
     	$mobile		= NULL;      	
     	$submit 	= NULL;
     	
     	extract($_POST);
     	$params['name']		=	$name;
     	$params['address']	=	$address; 
     	$params['phone']	=	$phone;
     	$params['mobile']	=	$mobile;    
     	
     	if(isset($submit))
     	{		
			$res=$this->EmergencyContacts_model->insertData($params);			
			 if($res)
	         {	         	
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('EmergencyContacts/index');		
	 }
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'id',
		'name',
		'address',
		'phone',
		'mobile'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->EmergencyContacts_model->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId			= NULL;
	 	$name			= NULL;
     	$address		= NULL;  
     	$phone			= NULL;
     	$mobile			= NULL; 
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$editId				=	$editId;
     	$params['name']		=	$name;
     	$params['address']	=	$address; 
     	$params['phone']	=	$phone;
     	$params['mobile']	=	$mobile;        
     	   	
     	if(isset($submit))
     	{			
			$res=$this->EmergencyContacts_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('EmergencyContacts/index');
	 }
  	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->EmergencyContacts_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('EmergencyContacts/index');  		
      }
}
