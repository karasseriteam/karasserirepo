<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Standing Committee Details
		</h1>
	</section>
	<section class="content">
	<?php
	if($this->session->flashdata('flash')){
		?>
		<div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
			<?= $this->session->flashdata('flash')['message']; ?>
		</div>
		<?php
	}
	?>
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">List of Standing Committee details</h3>
					<span class="pull-right"><a href="<?= site_url(); ?>/CommitteeDetails/add_view" class="btn btn-primary btn-flat">Add New</a></span>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-bordered">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Committee Name</th>                                 
							<th width="200px;">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if(count($records) > 0){
					$i =$this->uri->segment(3); 
					foreach($records as $data){
					$i++;
					?>
					<tr>
					<td><?= $i; ?></td>
					<td><?= $data->standingCommittee; ?></td>                          
					<td>
					<a href="<?= site_url(); ?>/CommitteeDetails/view/<?= $data->id; ?>" class="btn btn-primary btn-flat detailsView">View</a>
					<!-- <a href="<?= site_url(); ?>/CommitteeDetails/add/<?= $data->id; ?>" class="btn btn-primary btn-flat detailsView">Add Members</a>-->
					<!--
						<a href="#" data-toggle="modal" data-target="#addModal" class="btn btn-success btn-flat">Add Members</a>-->
						 
						<!-- Modal pop up  -->
						<div id="addModal" class="modal fade" role="dialog">
						<div class="modal-dialog">

						<!-- Modal content-->						
						<div class="modal-content">
							<div class="modal-header bg-blue">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Add Members</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="box box-primary">
											<div class="box-header">
												<h3 class="box-title"><?= $data->standingCommittee; ?></h3>
											</div>
											<div class="box-body">
												<div class="row">
													<div class="col-lg-8 col-md-8 col-sm-8">
														<div class="form-group">
															<label for="representativeId">Representative Name</label><span class="text-danger">*</span>
															<select name="representativeId" id="representativeId" class="form-control" required>
																<option value="">Select</option>
																<?php foreach($rep as $rr){
																	?>
																	<option value="<?= $rr->id;?>"><?= $rr->name;?></option>
																	<?php
																}
																?>
															</select>
														</div> 
														<div class="form-group">
															<label for="positionId">Position</label><span class="text-danger">*</span>
															<select name="positionId" id="positionId" class="form-control" required>
																<option value="">Select</option>
																<?php foreach($pos as $rp){
																	?>
																	<option value="<?= $rp->id;?>"><?= $rp->position;?></option>
																	<?php
																}
																?>
															</select>
														</div>                                 
														<div class="form-group">
															<button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
														</div>
                              
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
          
          
							<div class="box-body table-responsive no-padding">

							</div>        
						</div>
				</div>
			</div>
			<!-- Modal pop up end --> 
                                        
			</td>
			</tr>
			<?php                                   
			}
			} else{
				?>
				<tr><td colspan="5" align="center">No records found.</td></tr>
				<?php
			}
			?>
			</tbody>
			</table>
		</div>
	</div>
	<?php 
	$rowCount = count($records);
	?>
	<!-- for pagination --->
	<div class="row" align="center">
		<?php if($rowCount!=0){ echo $this->pagination->create_links();} else{}; ?>
	</div>
	<!-- pagination end -->
</div>
</div>
</section>
</div>
<!-- /.content-wrapper -->

<!-- Modal pop up  -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Members details</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered" align="center" width="100%">
					<thead>
						<tr>
							<th colspan="4"></th>
						</tr>
						<tr>
							<th>Sl No</th>
							<th>Name</th>
							<th>Position</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                	
					</tbody>                                                                      
				</table>
			</div>
          
          
			<div class="box-body table-responsive no-padding">

			</div>       
		</div>
	</div>
</div>
<!-- Modal pop up end -->
<script>	
	function delete_type()
	{
		var del=confirm("Do you Want to Delete ?");
		if(del==true)
		{
			window.submit();
		}
		else
		{
			return false;
		}
	}
</script>
<script>
	$(document).ready(function(){
			$('.detailsView').click(function(e) {
					e.preventDefault();
					var btn = $(this),
					url = btn.attr('href'),
					stdCom = btn.parent().prev().text(),
					popUp = $('#myModal');
					$.ajax({
							url: url,
							type: 'POST',
							success: function(result) {
								//console.log(jQuery.parseJSON(result));
								popUp.find('thead tr:first th').text(stdCom);
								popUp.find('tbody').empty();
								var i = 1;
								$.each(result, function(k, v) {
										popUp.find('tbody').append('<tr><td>'+ i +'</td><td>'+ v.name +'</td><td>'+ v.position +'</td><td><a href="<?= site_url(); ?>/CommitteeDetails/delete/'+ v.id +'" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a></td></tr>');
										i += 1;	
									});
								popUp.modal('show');
							}
						});
				});
		});
</script>