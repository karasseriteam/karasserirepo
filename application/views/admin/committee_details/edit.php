<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Standing Committee
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Standing Committee</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">                                
                                   <form action="<?= site_url(); ?>/CommitteeDetails/edit" method="post" >
                                    <?php foreach($results as $r){  ?>
                    			    <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                        <label for="standingComId">Standing Committee Name</label><span class="text-danger">*</span>
                                        <select name="standingComId" id="standingComId" class="form-control" required>
                                        	<option value="">Select</option>
                                        	<?php foreach($com as $rc)
                                        	{
												?>
												<option value="<?= $rc->id;?>"><?= $rc->standingCommittee;?></option>
												<?php
											}
                                        	?>
                                        </select>
                                    </div>  
                                    <div class="form-group">
                                        <label for="representativeId">Representative Name</label><span class="text-danger">*</span>
                                        <select name="representativeId" id="representativeId" class="form-control" required>
                                        	<option value="">Select</option>
                                        	<?php foreach($rep as $rr)
                                        	{
												?>
												<option value="<?= $rr->id;?>"><?= $rr->name;?></option>
												<?php
											}
                                        	?>
                                        </select>
                                    </div> 
                                    <div class="form-group">
                                        <label for="positionId">Position</label><span class="text-danger">*</span>
                                        <select name="positionId" id="positionId" class="form-control" required>
                                        	<option value="">Select</option>
                                        	<?php foreach($pos as $rp)
                                        	{
												?>
												<option value="<?= $rp->id;?>"><?= $rp->position;?></option>
												<?php
											}
                                        	?>
                                        </select>
                                    </div>                                 
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
function imgDisply(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('bannerImage').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('bannerImage').style.display='none';
	}
}
</script>
