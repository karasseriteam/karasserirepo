<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
          President / Vice president
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        
                        
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            
                            <tbody>
                            <?php if(count($records)>0){ 
                                $i = $this->uri->segment(3);
                                foreach ($records as $posdata) {
                                	if($posdata->positionId=='1'){
                                ?>
                                 <form action="<?php echo site_url(); ?>/rep_position/add" method="post" enctype="multipart/form-data">
                                <tr>
                                   
                                    <td> <div class="form-group">
                                        <label for="name1">Representative</label><span class="text-danger">*</span>
                                        <select name="name1" id="name1" class="form-control" required>
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($representatives as $rep_data) {
                                                ?>
                                                <option value="<?= $rep_data->id?>" <?php if($posdata->representativeId==$rep_data->id){echo "selected";}?> ><?= $rep_data->name; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <label for="position">Position</label><br>
                                    <?php
                                    foreach ($positions as $pos_data) {
                                        if($pos_data->id=='1'){
                                    ?>
                                       <input type="hidden" name="position1" id="position1" value="<?php echo $pos_data->id; ?>">
                                        <input type="text" name="posshow1" id="posshow1" value="<?php echo $pos_data->position; ?>" readonly="">
                                    <?php
                                    }}
                                    ?>
                                    </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit1">Update</button>
                                        </div>
                                    </td>
                                </tr>
                                </form>

                                <?php }if($posdata->positionId=='2'){?>
                                <form action="<?php echo site_url(); ?>/rep_position/add" method="post" enctype="multipart/form-data">
                                 <tr>
                                   
                                    <td> <div class="form-group">
                                        <label for="name2">Representative</label><span class="text-danger">*</span>
                                        <select name="name2" id="name2" class="form-control" required>
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($representatives as $rep_data) {
                                                ?>
                                                <option value="<?= $rep_data->id?>" <?php if($posdata->representativeId==$rep_data->id){echo "selected";}?> ><?= $rep_data->name; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <label for="position">Position</label><br>
                                    <?php
                                    foreach ($positions as $pos_data) {
                                        if($pos_data->id=='2'){
                                    ?>
                                       <input type="hidden" name="position2" id="position2" value="<?php echo $pos_data->id; ?>">
                                        <input type="text" name="posshow2" readonly="" id="posshow2" value="<?php echo $pos_data->position; ?>" >
                                    <?php
                                    }}
                                    ?>
                                    </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit2">Update</button>
                                        </div>
                                    </td>
                                </tr>
                                </form>
                                <?php 
                                } }}if($president<=0){
                                    ?>
                                     <form action="<?php echo site_url(); ?>/rep_position/add" method="post" enctype="multipart/form-data">
                                <tr>
                                   
                                    <td> <div class="form-group">
                                        <label for="name1">Representative</label><span class="text-danger">*</span>
                                        <select name="name1" id="name1" class="form-control" required>
                                            <?php
                                            foreach ($representatives as $rep_data) {
                                                ?>
                                                <option value="<?= $rep_data->id?>"><?= $rep_data->name; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <label for="position">Position</label><span class="text-danger">*</span><br>
                                    <?php
                                    foreach ($positions as $pos_data) {
                                        if($pos_data->id=='1'){
                                    ?>
                                       <input type="hidden" name="position1" id="position1" value="<?php echo $pos_data->id; ?>">
                                        <input type="text" name="posshow1" id="posshow1" value="<?php echo $pos_data->position; ?>" readonly="">
                                    <?php
                                    }}
                                    ?>
                                    </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit1">Update</button>
                                        </div>
                                    </td>
                                </tr>
                                </form>
                                <?php }if($vpresident<=0){?>
                               
                                <form action="<?php echo site_url(); ?>/rep_position/add" method="post" enctype="multipart/form-data">
                                 <tr>
                                   
                                    <td> <div class="form-group">
                                        <label for="name2">Representative</label><span class="text-danger">*</span>
                                        <select name="name2" id="name2" class="form-control" required>
                                            <?php
                                            foreach ($representatives as $rep_data) {
                                                ?>
                                                <option value="<?= $rep_data->id?>"><?= $rep_data->name; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <label for="position">Position</label><span class="text-danger">*</span><br>
                                    <?php
                                    foreach ($positions as $pos_data) {
                                        if($pos_data->id=='2'){
                                    ?>
                                       <input type="hidden" name="position2" id="position2" value="<?php echo $pos_data->id; ?>">
                                        <input type="text" name="posshow2" readonly="" id="posshow2" value="<?php echo $pos_data->position; ?>" >
                                    <?php
                                    }}
                                    ?>
                                    </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit2">Update</button>
                                        </div>
                                    </td>
                                </tr>
                                </form>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
          
            </div>
        </div>
    </section>
</div>
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}



</script>
<!-- /.content-wrapper -->
