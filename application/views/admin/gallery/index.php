<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Gallery
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                    	
                    	<?php   
                        $countOfGallery = count($records);
                        $countOfSubGallery = count($subGallery);
                        $totalCount = $countOfGallery+$countOfSubGallery;
                        if($totalCount<=30) {
						
                        ?>
                    
                        <h3 class="box-title">List of Gallery</h3>
                        
                        <span class="pull-right"><a href="<?= site_url(); ?>/Gallery/add_view" class="btn btn-primary btn-flat">Add New</a></span>
                        
                        <?php } else { ?>
                        
                        <h3 class="box-title">Image Limit Exceeded.! Delete Some Images and Try Again.!</h3>
                    <?php } ?>    
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Description</th>
                                <th>Gallery Photo</th>
                                <th width="250px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                            	$i =$this->uri->segment(3); 
                                foreach ($records as $data) {
                                	$i++;
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $data->description; ?></td>
                                        <td><img src="<?= base_url(); ?><?= $data->image;?>"></td>
										
                                        <td>
                                            <a href="<?= site_url(); ?>/Gallery/edit_view/<?= $data->id; ?>" class="btn btn-primary btn-flat">Edit</a>
                                            <a href="<?= site_url(); ?>/Gallery/delete/<?= $data->id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>
                                            <a href="#" data-toggle="modal" data-target="#myModal<?= $data->id; ?>" class="btn btn-success btn-flat">Add SubImage</a>
                                            
                                            
                                            <!-- Modal -->
											<div id="myModal<?php echo $data->id; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											   <form action="<?php echo site_url();?>/Sub_gallery/add_sub_image" method="post" enctype="multipart/form-data">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Add Sub Gallery</h4>
											      </div>
											      
											      <div class="modal-body">
											        <!--<p>Some text in the modal.</p>-->
											        <input type="hidden" name="galleryId" value="<?php echo $data->id; ?>">
											       
											        <div class="row">                               
						                               <div class="col-lg-4 col-md-4 col-sm-4">
						                                    <div class="form-group">
						                                        <label for="title">Sub Gallery<span class="text-danger">*</span></label> 
						                                        <input type="file" name="subImage" id="subImage" required="">
						                                    </div> 
						                               </div>                      
						                           </div>
						                         
											      </div>
											      <div class="modal-footer">
											        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
											      </div>
											      
						<div class="box-body table-responsive no-padding">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Sl No</th>
									<th>Sub Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php $j = 1; 
								foreach($subGallery as $subG){  
									if($data->id==$subG->galleryId)
									{
									?>
								<tr>
									<td><?php echo $j++; ?></td>
									<td><img src="<?= base_url(); ?><?= $subG->subImage;?>"></td>
									<td><a href="<?php echo site_url(); ?>/Sub_gallery/delete/<?php echo $subG->id;?>" class="btn btn-danger btn-flat"  onclick="return delete_type();">Delete
											</a>
									</td>
								</tr>
								<?php
									}
								 } ?>
							</tbody>
						</table>
											    </div>
												</form>
											  </div>
											</div>
                                            
                                            
                                        </td>
                                    </tr>
                                    <?php                                   
                                }
                            } else {
                                ?>
                                <tr><td colspan="4" align="center">No records found.</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
               <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>