<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Representatives
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Representatives</h3>
                        <span class="pull-right"><a href="<?= site_url(); ?>/representatives/add" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
	                            <tr>
	                                <th>S.No</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Mobile</th>
                                    <th>Party</th>
                                    <th>Ward Number</th>
                                    <th>Ward Name</th>
                                    <th>Option</th>
	                            </tr>
                            </thead>
                            <tbody>
                            <?php if(count($records)>0){ 
                                $i = $this->uri->segment(3);
                                foreach ($records as $repdata) {
                                    $i++
                                ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $repdata->name;?></td>
                                    <td><?php echo $repdata->address;?></td>
                                    <td><?php echo $repdata->mobile;?></td>
                                    <td><?php echo $repdata->party;?></td>
                                    <td><?php echo $repdata->wardNumber;?></td>
                                    <td><?php echo $repdata->wardName;?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#myModal<?php echo $repdata->id; ?>" class="btn btn-success btn-flat">View</a>
                                        <a href="<?= site_url(); ?>/representatives/edit/<?= $repdata->id; ?>" class="btn btn-primary btn-flat">Edit</a>
                                        <a href="<?php echo site_url(); ?>/representatives/delete/<?= $repdata->id; ?>" onclick="return delete_type()" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a> 

                                        <!-- Modal pop up end -->
                                        <div id="myModal<?php echo $repdata->id; ?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header bg-blue">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Representative details</h4>
                                                  </div>
                                                  
                                                  <div class="modal-body">
                                                   <table class="table table-bordered">
                            
                            <table class="table table-bordered" align="center" width="100%">
                                <tr>
                                    <th>Name</th>
                                    <th><?php echo $repdata->name; ?></th>
                                </tr>
                                <tr>
                                    <th>Age</th>
                                    <th><?php echo $repdata->age; ?></th>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <th><?php echo $repdata->gender; ?></th>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <th><?php echo $repdata->address; ?></th>
                                </tr>   
                                <tr>
                                    <th>Phone</th>
                                    <th><?php echo $repdata->phone; ?></th>
                                </tr>
                                <tr>
                                    <th>Mobile</th>
                                    <th><?php echo $repdata->mobile; ?></th>
                                </tr>
                                <tr>
                                    <th>Marital Status</th>
                                    <th><?php echo $repdata->maritalStatus; ?></th>
                                </tr>
                                <tr>
                                    <th>Education Qualification</th>
                                    <th><?php echo $repdata->eduQualification; ?></th>
                                </tr>   
                                
                                <tr>
                                    <th>Job</th>
                                    <th><?php echo $repdata->job; ?></th>
                                </tr>   
                                <tr>
                                    <th>Party</th>
                                    <th><?php echo $repdata->party; ?></th>
                                </tr>
                                <tr>
                                    <th>Ward Number</th>
                                    <th><?php echo $repdata->wardNumber; ?></th>
                                </tr>
                                <tr>
                                    <th>Ward Name</th>
                                    <th><?php echo $repdata->wardName; ?></th>
                                </tr>
                                <?php if($repdata->photo){
                                        ?>
                                    <tr>
                                        <th>Photo</th>
                                        <th><img src="<?php echo base_url();?>/<?php echo $repdata->photo; ?>"></th>          
                                    </tr>                            
                                    <?php
                                        }                                      
                                    ?>                                                  
                            </table>
                                                  </div>
                                                  
                                                  
                                                  <div class="box-body table-responsive no-padding">
                        
                                                  </div>
                                                </form>
                                              </div>
                                            </div>
                                        </div>
                                            <!-- Modal pop up end -->
                                    </td>
                                </tr>
                                <?php }} else{ ?>   
                                <tr>
                                	<td colspan="8" align="center">No records found.</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php 
                    $rowCount = count($records);
                     ?>
                    <!--  for pagination --> 
                    <div class="row" align="center">
                        <?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
                    </div>
                    <!-- pagination end -->
            </div>
        </div>
    </section>
</div>
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}



</script>
<!-- /.content-wrapper -->
