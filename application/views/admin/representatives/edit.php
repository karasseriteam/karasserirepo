<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Representative
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Update Representative</h3>
                    </div>
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/representatives/update" method="post" enctype="multipart/form-data">
                        <div class="row">
                        <?php foreach($results as $repdata){  ?>
                         <input type="hidden" name="editId" value="<?php echo $repdata['id'];?>">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                 <div class="form-group">
                                        <label for="name">Name</label><span class="text-danger">*</span>
                                        <input type="text" name="name" id="name" class="form-control" required value="<?php echo $repdata['name'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="gender">Gender</label><br>
                                        <input type="radio" name="gender" value="M" <?php if($repdata['genderEng']=="M"){?> checked="checked"<?php ;}?>>Male &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" <?php if($repdata['genderEng']=="F"){ ?> checked="checked"<?php ;}?> value="F">Female
                                    </div>
                                    <div class="form-group">
                                        <label for="age">Age</label>
                                        <input type="text" name="age" id="age" class="form-control" value="<?php echo $repdata['age'];?>" >
                                    </div>
                                     <div class="form-group">
                                        <label for="address">Address</label><span class="text-danger">*</span>
                                        <textarea name="address" id="address" class="form-control" required="" ><?php echo $repdata['address'];?></textarea > 
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" name="phone" id="phone" class="form-control" value="<?php echo $repdata['phone'];?>" >
                                    </div>
                                     <div class="form-group">
                                        <label for="mobile">Mobile</label><span class="text-danger">*</span>
                                        <input type="text" name="mobile" id="mobile" class="form-control" required="" value="<?php echo $repdata['mobile'];?>" >
                                    </div>
                                     <div class="form-group" style="width: 400px">
                                        <label for="marital_status">Marital Status</label><br>
                                       <input type="radio" name="marital" value="Y" <?php if($repdata['maritalStatusEng']=="Y"){?> checked="checked"<?php ;}?>>Married &nbsp;&nbsp;&nbsp;
                                       <input type="radio" value="N" <?php if($repdata['maritalStatusEng']=="N"){?> checked="checked"<?php ;}?> name="marital">Not Married &nbsp;&nbsp;&nbsp; 
                                       <input type="radio" name="marital" <?php if($repdata['maritalStatusEng']=="W"){?> checked="checked"<?php ;}?> value="W">Widdow
                                    </div>
                                     <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" name="qualification" id="qualification" class="form-control" value="<?php echo $repdata['eduQualification'];?>" >
                                    </div>
                                     <div class="form-group">
                                        <label for="job">Job</label>
                                        <input type="text" name="job" id="job" class="form-control"  value="<?php echo $repdata['job'];?>" >
                                    </div>
                                     <div class="form-group">
                                        <label for="party">Party</label>
                                        <input type="text" name="party" id="party" class="form-control" value="<?php echo $repdata['party'];?>" >
                                    </div>
                                     <div class="form-group">
                                        <label for="wardno">Ward Number</label><span class="text-danger">*</span>
                                        <input type="text" name="wardno" id="wardno" class="form-control" required="" value="<?php echo $repdata['wardNumber'];?>" >
                                    </div>
                                    <div class="form-group">
                                        <label for="wardname">Ward Name</label>
                                        <input type="text" name="wardname" id="wardname" class="form-control" value="<?php echo $repdata['wardName'];?>" >
                                    </div>
                                     <div class="form-group" style="display: none;">
                                        <label for="position">Position</label>
                                        <input type="text" name="position" id="position" class="form-control" value="<?php echo $repdata['position'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Image want to change..?</label><span class="text-danger">*</span>
                                       <input type="radio" name="changeImage" id="ImageChange"  value="yes" onclick="imgDisply(this.value)">Y
                                       <input type="radio" name="changeImage" id="ImageNoChange"  value="no" onclick="imgDisply(this.value)">N
                                    </div>
                                    <div class="form-group" style="display: none" id="photoId">
                                        <label for="photo">Photo</label>
                                        <input type="file" name="photo" data-action="show_thumbnail">
                                    </div>
                                   
                            </div>
                        </div>
                       <?php } ?>
                    </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
	function imgDisply(radioValue)
	{
		if(radioValue=='yes')
		{
			document.getElementById('photoId').style.display='block';
		}
		if(radioValue=='no')
		{
			document.getElementById('photoId').style.display='none';
		}
	}
</script>
