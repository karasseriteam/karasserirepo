<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Representative
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Representative</h3>
                    </div>
                    
                           <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
                            
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/representatives/add" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                 <div class="form-group">
                                        <label for="name">Name</label><span class="text-danger">*</span>
                                        <input type="text" name="name" id="name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="gender">Gender</label><br>
                                        <input type="radio" name="gender" value="M" checked="">Male &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="gender" value="F">Female
                                    </div>
                                    <div class="form-group">
                                        <label for="age">Age</label>
                                        <input type="text" name="age" id="age" class="form-control" >
                                    </div>
                                     <div class="form-group">
                                        <label for="address">Address</label><span class="text-danger">*</span>
                                        <textarea name="address" id="address" class="form-control" required="" ></textarea > 
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" name="phone" id="phone" class="form-control"  >
                                    </div>
                                     <div class="form-group">
                                        <label for="mobile">Mobile</label><span class="text-danger">*</span>
                                        <input type="text" name="mobile" id="mobile" class="form-control" required="" >
                                    </div>
                                     <div class="form-group" style="width: 400px">
                                        <label for="marital_status">Marital Status</label><br>
                                       <input type="radio" name="marital" value="Y" checked="">Married &nbsp;&nbsp;&nbsp;<input type="radio" value="N" name="marital">Not Married &nbsp;&nbsp;&nbsp; <input type="radio" name="marital" value="W">Widdow
                                    </div>
                                     <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" name="qualification" id="qualification" class="form-control" >
                                    </div>
                                     <div class="form-group">
                                        <label for="job">Job</label>
                                        <input type="text" name="job" id="job" class="form-control" >
                                    </div>
                                     <div class="form-group">
                                        <label for="party">Party</label>
                                        <input type="text" name="party" id="party" class="form-control" >
                                    </div>
                                     <div class="form-group">
                                        <label for="wardno">Ward Number</label><span class="text-danger">*</span>
                                        <input type="text" name="wardno" id="wardno" class="form-control" required="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="wardname">Ward Name</label>
                                        <input type="text" name="wardname" id="wardname" class="form-control" >
                                    </div>
                                     <div class="form-group" style="display: none;">
                                        <label for="position">Position</label>
                                        <input type="text" name="position" id="position" class="form-control" value="മെമ്പർ" >
                                    </div>
                                    <div class="form-group">
                                        <label for="photo">Uploade Photo </label>
                                        <input type="file" name="photo" data-action="show_thumbnail">
                                    </div>
                                   
                            </div>
                        </div>
                       
                    </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

