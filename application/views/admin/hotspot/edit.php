<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Hotspot
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Hotspot</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form action="<?= site_url(); ?>/Hotspot/edit" method="post"  enctype="multipart/form-data">
                                 <?php foreach($results as $r){  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                    			 <div class="row">
	                    			 <div class="col-lg-4 col-md-4 col-sm-4">
	                                    <div class="form-group">
	                                        <label for="heading">Heading</label><span class="text-danger">*</span>
	                                        <input type="text" name="heading" id="heading" class="form-control" required value="<?php echo $r['heading'];?>">
	                                    </div>
	                                </div> 
	                                <div class="col-lg-4 col-md-4 col-sm-4">
	                                    <div class="form-group">
	                                        <label for="description">Description</label>
	                                        <textarea name="description" id="description" class="form-control"><?php echo $r['description'];?></textarea>
	                                    </div>   
	                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                   <!--image1-->
                                    <div class="form-group">
                                        <label for="changeImage">Image 1 want to change..?</label><span class="text-danger">*</span>
                                       <input type="radio" name="changeImage" id="ImageChange"  value="yes" onclick="imgDisply(this.value)">Y
                                       <input type="radio" name="changeImage" id="ImageNoChange"  value="no" onclick="imgDisply(this.value)">N
                                    </div>
                                    <div  style="display: none" id="imageDivId">  
                                      <div class="form-group">
                                        <label for="image">Hotspot Image 1 (768 * 432)</label>
                                        <input type="file" name="photo" data-action="show_thumbnail">
                                      </div>                                   
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <!--image2-->
                                    <div class="form-group">
                                        <label for="changeImage2">Image 2 want to change..?</label><span class="text-danger">*</span>
                                       <input type="radio" name="changeImage2" id="ImageChange2"  value="yes" onclick="imgDisply2(this.value)">Y
                                       <input type="radio" name="changeImage2" id="ImageNoChange2"  value="no" onclick="imgDisply2(this.value)">N
                                    </div>
                                    <div  style="display: none" id="imageDivId2">  
                                      <div class="form-group">
                                        <label for="image">Hotspot Image 2 (768 * 432)</label>
                                        <input type="file" name="photo2" data-action="show_thumbnail">
                                      </div>                                   
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <!--image3-->
                                    <div class="form-group">
                                        <label for="changeImage3">Image 3 want to change..?</label><span class="text-danger">*</span>
                                       <input type="radio" name="changeImage3" id="ImageChange3"  value="yes" onclick="imgDisply3(this.value)">Y
                                       <input type="radio" name="changeImage3" id="ImageNoChange3"  value="no" onclick="imgDisply3(this.value)">N
                                    </div>
                                    <div  style="display: none" id="imageDivId3">  
                                      <div class="form-group">
                                        <label for="image">Hotspot Image 3 (768 * 432)</label>
                                        <input type="file" name="photo3" data-action="show_thumbnail">
                                      </div>                                   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </div>
                            </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
function imgDisply(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('imageDivId').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('imageDivId').style.display='none';
	}
}
function imgDisply2(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('imageDivId2').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('imageDivId2').style.display='none';
	}
}
function imgDisply3(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('imageDivId3').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('imageDivId3').style.display='none';
	}
}
</script>
