<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit Service Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Service Category</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Service/edit_service_category" method="post">
                                 <?php foreach($results as $r){
                                 	$id = $r['id']; 
                                 	$serviceOfficeId = $r['officeId'];
                                 	  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                       <label for="title">Office</label><span class="text-danger">*</span>
                                    	<select class="form-control" name="officeId" required>
                                    	<?php $office = $this->Service_model->getOffice($id); ?>
	                                	<option value="">Select</option>
	                                	<?php foreach($office as $o){ 
	                                	$officeId = $o->id;
	                                	 ?>
	                                	<option value="<?= $o->id; ?>" <?php if($officeId==$serviceOfficeId) echo 'selected';  ?>><?= $o->office;?></option>
	                                	<?php } ?>
                                		</select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Category</label><span class="text-danger">*</span>
                                        <input type="text" name="category" id="category" class="form-control" value="<?= $r['category']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
