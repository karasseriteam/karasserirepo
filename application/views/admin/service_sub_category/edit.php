<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit Service Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Service Category</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Service/edit_service_sub_category" method="post">
                                 <?php foreach($results as $r){
                                 	$id = $r['id']; 
                                 	$ScategoryId = $r['categoryId'];
                                 	  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                       <label for="title">Category</label><span class="text-danger">*</span>
                                    	<select class="form-control" name="categoryId" required>
                                    	<?php $category = $this->Service_model->getServiceCategory($id); ?>
	                                	<option value="">Select</option>
	                                	<?php foreach($category as $cat){ 
	                                		$categoryId = $cat->id;
	                                	 ?>
	                                	<option value="<?= $cat->id; ?>" <?php if($categoryId==$ScategoryId) echo 'selected';  ?>><?= $cat->category;?></option>
	                                	<?php } ?>
                                		</select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Sub Category</label><span class="text-danger">*</span>
                                        <input type="text" name="subCategory" id="subCategory" class="form-control" value="<?= $r['subCategory']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
