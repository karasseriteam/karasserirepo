<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit Service
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Services</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <form action="<?= site_url(); ?>/Service/edit_service" method="post">
                                 <?php foreach($results as $r){
                                 	$id = $r['id']; 
                                 	$categoryId = $r['categoryId']; 
                                 	  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                       <label for="title">Category</label><span class="text-danger">*</span>
                                    	<select class="form-control" name="categoryId" required>
                                    	<?php $category = $this->Service_model->getServiceCategory($id); ?>
	                                	<option value="">Select</option>
	                                	<?php foreach($category as $sCat){ 
	                                		$realSubCatId = $sCat->id;
	                                	 ?>
	                                	<option value="<?= $sCat->id; ?>" <?php if($realSubCatId==$categoryId) echo 'selected';  ?>><?= $sCat->category;?></option>
	                                	<?php } ?>
                                		</select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Sub Category</label>
                                        <input type="text" name="subCategory" id="subCategory" class="form-control" value="<?= $r['subCategory']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description</label>
                                        <textarea name="description" id="description" class="form-control"  rows="5"><?= $r['description']; ?></textarea> 
                                    </div>
                                     <div class="form-group">
                                        <label for="title">Link</label>
                                        <input type="text" name="link" id="link" class="form-control" value="<?= $r['link']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
