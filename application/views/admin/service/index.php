<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Service
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Services</h3>
                        <span class="pull-right"><a href="<?= site_url(); ?>/Service/service_add_view" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                            	$i =$this->uri->segment(3);
                                foreach ($records as $data) {
                                	
                                	$i++;
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $data->category; ?></td>
                                        <td><?= $data->subCategory; ?></td>
                                       
                                        <td><?php if($data->link) { ?><a href="<?= $data->link; ?>" target="_blank">Click Here</a><?php } else {} ?></td>
                                        <td style="width: 250px;">
                                        	<a href="#" data-toggle="modal" data-target="#myModal<?= $data->sId; ?>" class="btn btn-success btn-flat">View</a>
                                            <a href="<?= site_url(); ?>/Service/edit_service_view/<?= $data->sId; ?>" class="btn btn-primary btn-flat">Edit</a>
                                            <a href="<?= site_url(); ?>/Service/service_delete/<?= $data->sId; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>
                                            
                                            <!--- for view more button  -->
                                             
                                            
                                            
                                            <!-- Modal -->
											<div id="myModal<?php echo $data->sId; ?>" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											   <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title"><b>Services</b></h4>
											      </div>
											      
						<div class="box-body table-responsive no-padding">
						<table class="table table-bordered">
							
							<tbody>
								<tr>
                                  <th>Category</th>
                                  <td> : </td>
                                  <td><?= $data->category; ?></td>
                                </tr>
                                <tr>
                                  <th width="100px;">Sub Category</th>
                                  <td> : </td>
                                  <td><?= $data->subCategory; ?></td>
                                </tr>
                                <tr>
                                  <th >Description</th>
                                  <td> : </td>
                                  <td><?= $data->description; ?></td>
                                </tr>
                                <tr>
                                  <th >Link</th>
                                  <td> : </td>
                                  <td><?php if($data->link) { ?><a href="<?= $data->link; ?>" target="_blank">Click Here</a><?php } else {} ?></td>
                                </tr>
								
							</tbody>
						</table>
											    </div>
												
											  </div>
											</div>
                                        </td>
                                    </tr>
                                    <?php                                   
                                }
                            } else {
                                ?>
                                <tr><td colspan="7" align="center">No records found.</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                 <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>