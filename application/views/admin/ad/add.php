<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Ad
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Ad</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Ad/add" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="heading">Heading</label>
                                        <input type="text" name="heading" id="heading" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="link">Link</label><span class="text-danger">*</span>
                                        <input type="text" name="link" id="link" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="photo">Ad Image (518x90)</label><span class="text-danger">*</span>
                                        <input type="file" name="photo" id="photo" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
