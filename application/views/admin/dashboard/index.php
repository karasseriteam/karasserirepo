<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12"><h4>Welcome to Karassery panchayath admin panel</h4></div>
        </div>
        <p></p>
        <?php
/*        var_dump($_SESSION);
        */?>
        
        <div class="row">
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('emergency_contacts');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Emergency Contacts</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/EmergencyContacts/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('enquiry');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Enquiry</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-commenting-o"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Enquiry/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('gallery');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Gallery</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-image"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Gallery/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('news');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>News</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-newspaper-o"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/News/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('flash_news');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Flash News</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Flash_news/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('plan_details');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Plan Details</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-tasks"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Plan_details/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
         <div class="row">
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('standing_committee');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Standing Committee</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Committee/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('services');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Services</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-tasks"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Service/service_index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<?php
                    		$this->db->from('representatives');
							$query = $this->db->get();
							$rowcount = $query->num_rows();     	
           	 			?>
                        <h3><?php echo $rowcount; ?></h3>
                        <p>Representatives</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <a href="<?php echo base_url(); ?>index.php/Representative/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>
</div>