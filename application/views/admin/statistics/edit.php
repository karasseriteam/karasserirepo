<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit Statistics
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Statistics</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Statistics/edit" method="post">
                                 <?php foreach($results as $r){
                                 	
                                 	  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                        <label for="title">Type</label><span class="text-danger">*</span>
                                        <input type="text" name="type" id="type" class="form-control" required value="<?= $r['type']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Total Count</label>
                                        <input type="text" name="totalCount" id="totalCount" class="form-control" value="<?= $r['totalCount']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
