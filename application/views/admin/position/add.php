<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Position
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Position</h3>
                    </div>
                    <div class="box-body">
                    <form action="<?php echo site_url(); ?>/position/add" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                 <div class="form-group">
                                        <label for="position">Position</label><span class="text-danger">*</span>
                                        <input type="text" name="position" id="position" class="form-control" required>
                                    </div>
                                   
                                     <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                            </div>
                            
                        </div>
                       
                    </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

