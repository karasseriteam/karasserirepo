<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit PMC
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit PMC</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Pmc/edit" method="post">
                                 <?php foreach($results as $r){
                                 	
                                 	  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                        <label for="title">Name</label><span class="text-danger">*</span>
                                        <input type="text" name="name" id="name" class="form-control" required value="<?= $r['name']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Position</label>
                                        <input type="text" name="position" id="position" class="form-control" value="<?= $r['position']; ?>">
                                    </div>
                                   <div class="form-group">
                                        <label for="title">Phone<span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control" name="phone" id="phone" required="" value="<?php echo $r['phone']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Address</label>
                                         <textarea class="form-control" name="address" id="address"><?= $r['address'] ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
