<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            PMC
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add PMC</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Pmc/add" method="post">
                                    <div class="form-group">
                                        <label for="title">Name</label><span class="text-danger">*</span>
                                        <input type="text" name="name" id="name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Position</label>
                                        <input type="text" name="position" id="position" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Phone</label><span class="text-danger">*</span>
                                        <input type="text" name="phone" id="phone" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Address</label>
                                         <textarea class="form-control" name="address" id="address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
