<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Add News
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add News</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/News/add" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="title">Heading</label><span class="text-danger">*</span>
                                        <input type="text" name="heading" id="heading" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description</label><span class="text-danger">*</span>
                                        <input type="text" name="description" id="description" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">News Date</label><span class="text-danger">*</span>
                                        <input type="text" name="newsDate" id="newsDate" class="form-control datepicker" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Image</label><span class="text-danger">*</span>
                                        <input type="file" name="imageUrl" id="imageUrl" class="form-control" required>
                                    </div>
                                     <!--<div class="form-group">
                                        <label for="title">Set As Flash News:</label>
                                        &nbsp;&nbsp;
                                        <label>No</label>
										<input type="radio" name="setFlash" value="no" checked="" /> 
										&nbsp;&nbsp;	
										<label>Yes</label>
										<input type="radio" name="setFlash" value="yes" />
                                    </div>-->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
