<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit News
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit News</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/News/edit" method="post"  enctype="multipart/form-data">
                                 <?php foreach($results as $r){
                                  $newsDate= $r['newsDate'];
                    			  $setnewsDate=date('d-m-Y', strtotime($newsDate));	
                                 	
                                 	  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                        <label for="title">Heading</label><span class="text-danger">*</span>
                                        <input type="text" name="heading" id="heading" class="form-control" value="<?= $r['heading']; ?>" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description</label><span class="text-danger">*</span>
                                        <input type="text" name="description" id="description" class="form-control" required value="<?= $r['description']; ?>">
                                    </div>
                                   <div class="form-group">
                                        <label for="title">News Date  <span class="text-danger">*</span></label> 
                                        <input type="text" class="form-control datepicker" placeholder="News Date" name="newsDate" id="newsDate" required="" value="<?php echo $setnewsDate; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Image want to change..?</label><span class="text-danger">*</span>
                                       <input type="radio" name="changeImage" id="ImageChange"  value="yes" onclick="imgDisply(this.value)">Y
                                       <input type="radio" name="changeImage" id="ImageNoChange"  value="no" onclick="imgDisply(this.value)">N
                                    </div>
                                    <div class="form-group" style="display: none" id="ImageUrl">
                                        <label for="property_image">Image (768x576)</label>
                                        <input type="file" name="imageUrl" data-action="show_thumbnail">
                                    </div>
                                    <!--<div class="form-group">
                                        <label for="title">Set As Flash News:</label>
                                        &nbsp;&nbsp;
                                        <label>No</label>
										<input type="radio" name="setFlash" value="no" checked="" <?php if($r['setFlash']=='no'){ echo('checked');} ?> /> 
										&nbsp;&nbsp;	
										<label>Yes</label>
										<input type="radio" name="setFlash" value="yes" <?php if($r['setFlash']=='yes'){ echo('checked');} ?> />
                                    </div>-->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
function imgDisply(radioValue)
{
	if(radioValue=='yes')
	{
		document.getElementById('ImageUrl').style.display='block';
	}
	if(radioValue=='no')
	{
		document.getElementById('ImageUrl').style.display='none';
	}
}
</script>
