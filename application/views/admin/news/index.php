<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           News
        </h1>
    </section>
    <section class="content">
        <?php
        if ($this->session->flashdata('flash')) {
            ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of News</h3>
                        <span class="pull-right"><a href="<?= site_url(); ?>/News/add_news" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Heading</th>
                                <!--<th>Description</th>-->
                                <th>Date</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                            	$i =$this->uri->segment(3);
                                foreach ($records as $data) {
                                	$i++;
                                	
                                	$newsDate= $data->newsDate;
                    			    $setnewsDate=date('d-m-Y', strtotime($newsDate));
                                	
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $data->heading; ?></td>
                                        <!--<td><?= $data->description; ?></td>-->
                                        <td  style="width: 100px;"><?= $setnewsDate ?></td>
                                        <td><img src="<?= base_url(); ?><?= $data->imageUrl;?>"></td>
                                        <td style="width: 10%;"> 
                                        <a href="#" data-toggle="modal" data-target="#myModal<?= $data->id; ?>" class="btn btn-success btn-flat">View</a>                               
                                            <a href="<?= site_url(); ?>/News/edit_news/<?= $data->id; ?>" class="btn btn-primary btn-flat">Edit</a>
                                            <a href="<?= site_url(); ?>/News/delete_news/<?= $data->id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>
                                            
<!--Model start-->
<div id="myModal<?php echo $data->id; ?>" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width:80%">
	<!-- Modal content-->
	<form method="post">
    	<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title">News details</h4>
	      	</div>							      		    	    
			<div class="box-body table-responsive no-padding">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Heading</th>
							<th>:</th>
							<th><?= $data->heading; ?></th>
						</tr>
						<tr>
							<th>Description</th>
							<th>:</th>
							<th><?= $data->description; ?></th>
						</tr>
						<?php 
						if($data->imageUrl) {
						?>
						<tr>
							<th>Image</th>
							<th>:</th>
							<th><img src="<?= base_url(); ?><?= $data->imageUrl;?>" width="25%" height="25%"></th>
						</tr>
						<?php			
						}
						?>
					</thead>
					
				</table>
			</div>
		</div>
	</form>
</div>
</div>
<!--model end-->
                                        </td>
                                    </tr>
                                    <?php                                   
                                }
                            } else {
                                ?>
                                <tr><td colspan="5" align="center">No records found.</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                 <?php 
                  	$rowCount = count($records);
                  	 ?>
                  	<!-- for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
            </div>
        </div>
    </section>
</div>
		
<!-- /.content-wrapper -->
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>